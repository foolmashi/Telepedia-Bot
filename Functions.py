import database
from fuzzywuzzy import fuzz
from datetime import datetime
import json
import logging

db = database.connect()
messagesCollection = db["messages"]
usersCollection = db["users"]

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s',
    level=logging.INFO)
logger = logging.getLogger(__name__)


def getSupportText(key):
    try:
        f = open('supportText.json', "r")
        data = json.loads(f.read())
        f.close()
        return str(data[key])
    except Exception as er:
        logger.error(er)


def getAnswer(question):
    return ""


def getSimilarity(text1, text2):
    score = fuzz.ratio(text1, text2)
    return int(score) or 0


def saveAnswer(questionId, answer, similarity):
    answer = {
        "answerText": str(answer),
        "similarityScore": similarity,
        "dateCreated": datetime.today()
    }
    return
