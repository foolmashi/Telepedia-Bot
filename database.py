"""
    Raffle Bot: Database Connection
    Use this function to connect to the database of the bot. Keys are saved as Heroku env variables
    author: Asiri H
    date: 25 Sept 2021
"""
import os
import pymongo
import logging

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s',
    level=logging.INFO)
logger = logging.getLogger(__name__)


# connect MongoDB
def connect():
    try:
        connectionURL = os.environ.get('connectionURL',
                                       "mongodb+srv://botaUser:botaPassword@botscluster.j5qlm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        databaseName = os.environ.get('databaseName', "AthalBota")
        dbClient = pymongo.MongoClient(connectionURL)
        if dbClient:
            logger.info("Database Client initialized.")
            database = dbClient[str(databaseName)]
            if database:
                logger.info("Database Connected.")
                return database
            else:
                return None
        else:
            return None
    except Exception as er:
        logger.error(er)
