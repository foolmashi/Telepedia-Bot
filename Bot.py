import os, psycopg2, time
from psycopg2 import sql, connect
from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton

app = Client("bot")

def connect():
    return psycopg2.connect(
    host="ec2-44-198-154-255.compute-1.amazonaws.com",
    user="rbtiiueqxfcupb",
    password="070638dcdd6abe397fd9fd0f525fd43a1d89fe4178f494d35e389d31c67bd84a",
    database="dd3t0btjcjetdk"
    )

def register_user(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"INSERT INTO users (id, rank) VALUES ({user_id}, 0) ON CONFLICT (id) DO UPDATE SET id = {user_id};"
        cur.execute(sql)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        print(e)

def add_user(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"UPDATE users SET rank = 1 WHERE id = {user_id}"
        cur.execute(sql)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        print(e)

def remove_user(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"UPDATE users SET rank = 0 WHERE id = {user_id}"
        cur.execute(sql)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        print(e)

def add_admin(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"UPDATE users SET rank = 2 WHERE id = {user_id}"
        cur.execute(sql)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        print(e)

def remove_admin(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"UPDATE users SET rank = 0 WHERE id = {user_id}"
        cur.execute(sql)
        db.commit()
        cur.close()
        db.close()
    except Exception as e:
        print(e)

def is_user(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"SELECT * FROM users WHERE id = {user_id} AND rank > 0"
        cur.execute(sql)
        count = cur.rowcount
        db.commit()
        cur.close()
        db.close()
        if count > 0:
            return True
        else:
            return False
    except Exception as e:
        print(e)

def is_admin(user_id):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"SELECT * FROM users WHERE id = {user_id} AND rank > 1"
        cur.execute(sql)
        count = cur.rowcount
        db.commit()
        cur.close()
        db.close()
        if count > 0:
            return True
        else:
            return False
    except Exception as e:
        print(e)

def search_in_db(query):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"SELECT * FROM carmdi WHERE {query}"
        #val = (query,)
        cur.execute(sql)
        res = cur.fetchall()
        db.commit()
        cur.close()
        db.close()
        if res is not None:
            return res
        else:
            return None
    except Exception as e:
        print(e)

def get_columns_names(table):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"SELECT column_name FROM information_schema.columns WHERE table_name = '{table}';"
        #val = (query,)
        cur.execute(sql)
        res = cur.fetchall()
        db.commit()
        cur.close()
        db.close()
        if res is not None:
            return res
        else:
            return None
    except Exception as e:
        print(e)

def count_row_db(query):
    try:
        db = connect()
        cur = db.cursor()
        sql = f"SELECT * FROM carmdi WHERE {query}"
        #val = (user_id)
        cur.execute(sql)
        count = cur.rowcount
        res = cur.fetchall()
        cur.close()
        db.close()
        return count
    except Exception as e:
        print(e)

@app.on_message(filters.private)
async def check_msg(Client, message):
    try:
        register_user(message.from_user.id)
    except:
        pass
    if is_user(message.from_user.id) == False:
        await app.send_message(message.from_user.id, 'You are not able to use this bot!')
        message.stop_propagation()

@app.on_message(filters.private & filters.command(['adduser'], ['/']), group=2)
async def adduser(Client, message):
    if is_admin(message.from_user.id) == True:
        id_admin = message.text.replace('/adduser', '')
        add_user(id_admin)
        await app.send_message(message.from_user.id, f'New user {id_admin} added!')

@app.on_message(filters.private & filters.command(['removeuser'], ['/']), group=2)
async def removeuser(Client, message):
    if is_admin(message.from_user.id) == True:
        id_admin = message.text.replace('/removeuser', '')
        remove_user(id_admin)
        await app.send_message(message.from_user.id, f'User {id_admin} removed!')

@app.on_message(filters.private & filters.command(['addadmin'], ['/']), group=2)
async def addadmin(Client, message):
    if is_admin(message.from_user.id) == True:
        id_admin = message.text.replace('/addadmin', '')
        add_admin(id_admin)
        await app.send_message(message.from_user.id, f'New admin {id_admin} added!')

@app.on_message(filters.private & filters.command(['removeadmin'], ['/']), group=2)
async def removeadmin(Client, message):
    if is_admin(message.from_user.id) == True:
        id_admin = message.text.replace('/removeadmin', '')
        remove_admin(id_admin)
        await app.send_message(message.from_user.id, f'Admin {id_admin} removed!')

@app.on_message(filters.private & filters.command(['start'], ['/']), group=2)
async def start(Client, message):
    f = open(f'data/{message.from_user.id}_search_query.txt', 'w')
    keyboard = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(text="🔍 SEARCH IN DB 🔍", callback_data=f"!searchindb")
            ]
        ]
    )
    await app.send_message(message.from_user.id, f'Hi {message.from_user.mention}\n\n<i>Choose one of the option below.</i>', reply_markup=keyboard)

@app.on_message(filters.private, group=2)
async def insert_query(Client, message):
    f_status = open(f'data/{message.from_user.id}_status.txt', 'r+')
    if f_status.read() == 'SEARCH':
        f_status.write("START")
        f_status.close()

        f_column_name = open(f'data/{message.from_user.id}_column.txt', 'r+')

        column_name = f_column_name.read()

        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(text="Is", callback_data=f"!search {column_name}='{message.text}'")
                ],
                [
                    InlineKeyboardButton(text="Contain", callback_data=f"!search {column_name}≃LIKE≃'%{message.text}%'")
                ],
                [
                    InlineKeyboardButton(text="Starts With", callback_data=f"!search {column_name}≃LIKE≃'{message.text}%'")
                ],
                [
                    InlineKeyboardButton(text="Ends With", callback_data=f"!search {column_name}≃LIKE≃'%{message.text}'")
                ]
            ]
        )

        f_column_name.write('')
        f_column_name.close()

        await app.send_message(message.from_user.id, f'Ok {message.from_user.mention}, choose the search oerator from the options below:', reply_markup=keyboard)

@app.on_callback_query(group=2)
async def callback_query(Client, Query):
    if Query.data.startswith("!search "):
        search_query = Query.data.replace('!search ', '').replace('≃', ' ')
        if os.stat(f'data/{Query.from_user.id}_search_query.txt').st_size != 0:
            column_name = open(f'data/{Query.from_user.id}_column.txt', 'r+').read()
            f_search_query = open(f'data/{Query.from_user.id}_search_query.txt', "a")
            f_search_query.write(f" AND {search_query}")
            f_search_query.close()
        else:
            column_name = open(f'data/{Query.from_user.id}_column.txt', 'r+').read()
            f_search_query = open(f'data/{Query.from_user.id}_search_query.txt', "w")
            f_search_query.write(search_query)
            f_search_query.close()
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(text="🔍 Filter Again 🔍", callback_data=f"!searchindb")
                ],
                [
                    InlineKeyboardButton(text="❌ Cancel ❌", callback_data=f"!cancel"),
                    InlineKeyboardButton(text="✅ Show Results ✅", callback_data=f"!showresults")
                ]
            ]
        )
        current_query = open(f'data/{Query.from_user.id}_search_query.txt', "r").read()
        row_count = count_row_db(current_query)
        await Query.message.edit(f'Ok {Query.from_user.mention}, what you want to do now?\n\nThe query: <code>{current_query}</code> returned {row_count} results\n\n<i>Choose one of the option below:</i>', reply_markup=keyboard)

    if Query.data == "!searchindb":
        button = []
        #for column_name in get_columns_names("carmdi"):
        button.append(
            [
                InlineKeyboardButton(text="🔢 ActualNB", callback_data=f"!select_column \"ActualNB\""),
                InlineKeyboardButton(text="Letter 🆎", callback_data=f"!select_column \"codedesc\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="📅 Year Produced", callback_data=f"!select_column \"PRODDATE\""),
                InlineKeyboardButton(text="Chassis Number 🔠🔢", callback_data=f"!select_column \"Chassis\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="🔢🔠 Motor Number", callback_data=f"!select_column \"Moteur\""),
                InlineKeyboardButton(text="Date Aqquired 📆", callback_data=f"!select_column \"dateaquisition\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="🌈 Colour", callback_data=f"!select_column \"CouleurDesc\""),
                InlineKeyboardButton(text="Brand 🚗", callback_data=f"!select_column \"MarqueDesc\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="🏎 Model", callback_data=f"!select_column \"TypeDesc\""),
                InlineKeyboardButton(text="Utility Type 🚖", callback_data=f"!select_column \"UtilisDesc\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="🙋‍♂ First Name", callback_data=f"!select_column \"Prenom\""),
                InlineKeyboardButton(text="Last Name 👨‍👨‍👧‍👧", callback_data=f"!select_column \"Nom\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="🏠 Address", callback_data=f"!select_column \"Addresse\""),
                InlineKeyboardButton(text="Mother's Name 🤰", callback_data=f"!select_column \"NomMere\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="📱 Phone Number", callback_data=f"!select_column \"TelProp\""),
                InlineKeyboardButton(text="Civil Number 📜", callback_data=f"!select_column \"NoRegProp\"")
            ]
        )
        button.append(
            [
                InlineKeyboardButton(text="👶 Birthplace 👶", callback_data=f"!select_column \"BirthPlace\"")
            ]
        )

        button.append([InlineKeyboardButton(text="❌ Cancel ❌", callback_data="!cancel")])
        keyboard = InlineKeyboardMarkup(button)
        f = open(f'data/{Query.from_user.id}_status.txt', 'w')
        f.write("START")
        f.close()
        await Query.message.edit("Select column name:", reply_markup=keyboard)

    if Query.data.startswith("!select_column "):
        column_name = Query.data.replace("!select_column ", "")
        f = open(f'data/{Query.from_user.id}_column.txt', 'w')
        f.write(column_name)
        f.close()

        f = open(f'data/{Query.from_user.id}_status.txt', 'w')
        f.write("SEARCH")
        f.close()

        await Query.message.edit("What would you like to search for?")

    if Query.data == "!showresults":
        f = open(f'data/{Query.from_user.id}_status.txt', 'w')
        f.write("START")
        f.close()
        current_query = open(f'data/{Query.from_user.id}_search_query.txt', "r").read()
        search_in_db_r = search_in_db(current_query)
        keyboard = InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(text="BACK", callback_data=f"!cancel")
                    ]
                ]
            )

        #columns_name = get_columns_names("carmdi")

        if search_in_db_r != None:
            await Query.message.edit("Results for your research:")
            for count_row, search_in_db_r_row in enumerate(search_in_db_r):
                text = f"Plate Number 🔢: {search_in_db_r_row[0]}\nLetter 🆎: {search_in_db_r_row[1]}\nYear Produced 📅: {search_in_db_r_row[2]}\nChassis Number 🔠🔢: {search_in_db_r_row[3]}\nMotor Number 🔢🔠: {search_in_db_r_row[4]}\nDate Aqquired 📆: {search_in_db_r_row[5]}\nColour 🌈: {search_in_db_r_row[7]}\nBrand 🚗: {search_in_db_r_row[8]}\nModel 🏎: {search_in_db_r_row[9]}\nUtility Type 🚖_: {search_in_db_r_row[10]}\nFirst Name 🙋‍♂: {search_in_db_r_row[11]}\nLast Name 👨‍👨‍👧‍👧: {search_in_db_r_row[12]}\nAddress 🏠: {search_in_db_r_row[13]}\nMother's Name 🤰: {search_in_db_r_row[14]}\nPhone Number 📱: {search_in_db_r_row[15]}\nCivil Number 📜: {search_in_db_r_row[16]}\nBirthplace 👶: {search_in_db_r_row[17]}"

                """
                for count_column, search_in_db_r_row_column in enumerate(search_in_db_r_row):
                    if count_column == 0:
                        text += str(columns_name[count_column][0]) + ": " + str(search_in_db_r_row_column)
                    else:
                        text += '\n' + str(columns_name[count_column][0]) + ": " + str(search_in_db_r_row_column)
                """
                await app.send_message(Query.from_user.id, f"<code>{text}</code>")
                time.sleep(1)
            await app.send_message(Query.from_user.id, "<b>👆DONE👆</b>", reply_markup=keyboard)
        else:
            await Query.message.edit(f"NO results for your filter.", reply_markup=keyboard)


    if Query.data == "!cancel":
        f = open(f'data/{Query.from_user.id}_column.txt', 'w')

        f = open(f'data/{Query.from_user.id}_status.txt', 'w')
        f.write("START")
        f.close()

        f = open(f'data/{Query.from_user.id}_search_query.txt', 'w')
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(text="🔍 SEARCH IN DB 🔍", callback_data=f"!searchindb")
                ]
            ]
        )
        await Query.message.edit(f'Hi {Query.from_user.mention}\n\n<i>Choose one of the option below.</i>', reply_markup=keyboard)

app.run()
